# TrackGpo_SMTP

This is a companion project to the TrackGpo module. This module will email any changes to group policies to the SMTP server specified in your config/appsettings file.

## Description

This module enables you to easily extend the TrackGpo module by allowing you to receive SMTP messages from `Invoke-GpoTracking`.

You can override the subject line that will get used on SMTP messages, but I didn't make it dynamic at all. Let me know if you wish it was dynamic.

## Installation
If you have the [PowerShellGet](https://msdn.microsoft.com/powershell/gallery/readme) module installed you can enter the following command:

    Install-Module TrackGpo_Smtp

## Examples
Basic add for gmail settings:

    $null = New-TrackGpoSmtpConfig -SmtpServer smtp.gmail.com -Port 587 -From $Gmail_Cred.UserName -To you@gmail.com -UseSsl -Credential $Gmail_cred
    Invoke-GpoTracking ...

And as a splat:

    $Splat = @{
        SmtpServer = "smtp.gmail.com"
        Port       = 587
        From       = $Gmail_Cred.UserName
        To         = "you@gmail.com"
        UseSsl     = $true
        Credential = $Gmail_cred
    }
    $null = New-TrackGpoSmtpConfig @Splat
    Invoke-GpoTracking ...

And for an SMTP server that uses no authentication:

    $Splat = @{
        SmtpServer = "mail.domain.test"
        From       = foo
        To         = "you@gmail.com"
    }
    $null = New-TrackGpoSmtpConfig @Splat
    Invoke-GpoTracking ...
