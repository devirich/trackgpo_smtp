function New-TrackGpoError_External {
    [CmdletBinding()]
    param(
        [parameter(Mandatory)]
        [String]$Message,

        [hashtable]$SmtpConfig = $Global:SmtpConfig
    )
    $Splat = @{}
    if (-not $SmtpConfig) { Write-Warning "No SMTP options configured. See Help for more details" }
    elseif (-not $SmtpConfig.ContainsKey("From")) { Write-Warning "No SMTP 'From' param configured. Skipping."; return }
    elseif (-not $SmtpConfig.ContainsKey("To")) { Write-Warning "No SMTP 'To' param configured. Skipping."; return }
    if (-not $SmtpConfig.ContainsKey("Subject")) { $Splat.Add("Subject", "Processing Invoke-GpoTracking has encountered an error at $(Get-Date)") }
    Send-MailMessage @SmtpConfig @Splat -Body $Message
}
