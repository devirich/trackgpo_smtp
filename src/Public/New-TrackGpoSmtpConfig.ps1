﻿function New-TrackGpoSmtpConfig {
    <#
.SYNOPSIS
This function creates an Smtp config object and stores it to the variable SmtpOptions for use with the other cmdlets in this module.
Once you run this command and create the SmtpConfig object into your session, you're done with this and can run Invoke-GpoTracking.

.DESCRIPTION
The parameters for this command are pulled from the params from Send-MailMessage, which is what the other functions in this module use.

.PARAMETER SmtpServer
The SmtpServer to connect to when sending an email

.PARAMETER Port
The port to use for sending an email

.PARAMETER From
REQUIRED for Send-MailMessage. The address the email will come from

.PARAMETER To
REQUIRED for Send-MailMessage. The address the email will send to

.PARAMETER Cc
Other contacts to get a copy

.PARAMETER Bcc
Other contacts to get BCC'd

.PARAMETER ReplyTo
ReplyTo param for Send-MailMessage

.PARAMETER Subject
Static Subject to use instead of the default generated one

.PARAMETER UseSsl
Param to toggle UseSsl in Send-MailMessage

.PARAMETER Credential
Credentials to use for sending email via Send-MailMessage

.EXAMPLE
PS> New-TrackGpoSmtpConfig -SmtpServer "mail.domain.test" -From "admin@domain.test" -To "user@domain.test"

Creates an SmtpConfig hashtable and stores to $SmtpConfig. You can store the returned object and edit it further and overwrite the contents of that variable if you need to.

.EXAMPLE
PS> $null = New-TrackGpoSmtpConfig -SmtpServer smtp.gmail.com -Port 587 -From $Gmail_Cred.UserName -To user@gmail.com -UseSsl -Credential $Gmail_cred

Creates an SmtpConfig hashtable and stores to $SmtpConfig. For use with gmail SMTP server.

.NOTES
General notes
#>
    [CmdletBinding()]
    param (
        [parameter(Mandatory)]
        [string]$SmtpServer,
        [int]$Port = 22,
        [string]$From,
        [string[]]$To,
        [string[]]$Cc,
        [string[]]$Bcc,
        [string[]]$ReplyTo,
        [string]$Subject,
        [switch]$UseSsl,
        [switch]$BodyAsHtml,
        [PSCredential]$Credential
    )

    $Splat = [ordered]@{}
    (
        "SmtpServer",
        "Port",
        "From",
        "To",
        "Cc",
        "Bcc",
        "ReplyTo",
        "Subject",
        "UseSsl",
        "BodyAsHtml",
        "Credential"
    ) | ForEach-Object {
        if ($PSBoundParameters.ContainsKey($_)) { $Splat.Add($_, $PSBoundParameters[$_]) }
    }
    $Splat.Port = $Port
    $Global:SmtpConfig = $Splat
    $Global:SmtpConfig
}
# All Send-MailMessage params:
# "Attachments",
# "Bcc",
# "Body",
# "BodyAsHtml",
# "Encoding",
# "Cc",
# "DeliveryNotificationOption",
# "From",
# "SmtpServer",
# "Priority",
# "ReplyTo",
# "Subject",
# "To",
# "Credential",
# "UseSsl",
# "Port"
